package bsa.java.concurrency.fs;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface FileSystem {
    Future<String> saveFile(byte[] file, UUID id);

    Future<String> delete(UUID imageId);

    String getHTTPUrlById(UUID id);

    Future<?> deleteAll();
}
