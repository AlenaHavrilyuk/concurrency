package bsa.java.concurrency.fs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileSystemService implements FileSystem{
  @Autowired
  ExecutorService executorService;

  public final File IMAGE_FOLDER = new File(File.separator + "images");


  @Override
  public Future<String> saveFile(byte[] file, UUID id) {
    if (!IMAGE_FOLDER.exists()){
      System.out.println("dose not exist");
      if(IMAGE_FOLDER.mkdir()) {
        System.out.println("create folder");
      }
    }
    return  executorService.submit(() -> {
      BufferedOutputStream bos;
      FileOutputStream fos = new FileOutputStream(new File(getPathById(id)));

      bos = new BufferedOutputStream(fos);
      bos.write(file);
      return getHTTPUrlById(id);
    });
  }

//  @Override
//  public CompletableFuture<String> searchById(UUID id) {
//    return (CompletableFuture<String>) executorService.submit(() -> {
//      File file = new File(getPathById(id));
//      if (file.exists()) {
//        return getHTTPUrlById(id);
//      }
//      throw new NoSuchElementException();
//    });
//  }

  @Override
  public Future<String> delete(UUID id) {
    return executorService.submit(() -> {
      File file = new File(getPathById(id));
      if (file.exists()) {
        if(file.delete()){
          return " File deleted ";
        }
        return " Cannot delete file ";
      }
      return " File dose not exist ";
    });

  }

  @Override
  public String getHTTPUrlById(UUID id){
    return "http://127.0.0.1:" + "${server.port}" + getPathById(id);
  }

  @Override
  public Future<?> deleteAll() {
    return executorService.submit(() -> {
    if (IMAGE_FOLDER.exists()){
      File[] files = IMAGE_FOLDER.listFiles();
      assert files != null;
      Arrays.stream(files).parallel().forEach(File::delete);
    }
    });
  }

  private String getPathById(UUID id){
    return IMAGE_FOLDER + File.separator + id + ".jpg";
  }
}
