package bsa.java.concurrency.configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TreadConfigurator {

  @Bean
  public ExecutorService executorService(){
    return Executors.newCachedThreadPool();
  }
}
