package bsa.java.concurrency.hash;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HashService {
  @Autowired
  ExecutorService executorService;
  @Autowired
  DHasher hasher;

  public Future<Long> calculateHashAsynch(byte[] image){
    return executorService.submit(() -> hasher.calculateHash(image));
  }
}
