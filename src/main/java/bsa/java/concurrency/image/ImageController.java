package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import java.util.Arrays;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ImageController {
    @Autowired
    ImageService imageService;

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        logger.info("Saving new batch of images.");
        Arrays.stream(files).parallel().forEach(imageService::batchUploadImages);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
        @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        logger.info("Searching matches");
        Optional<List<SearchResultDTO>> optional = imageService.searchMatches(file, threshold);
        if (optional.isEmpty()){
            logger.warn("No matches is found");
            return null;
        }
        List<SearchResultDTO> resultDTOList = optional.get();
        logger.info("Found " + resultDTOList.size() + " matches.");
        return resultDTOList;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        logger.info("Deleting image by ID : " + imageId);
        imageService.delete(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        imageService.deleteAll();
    }
}
