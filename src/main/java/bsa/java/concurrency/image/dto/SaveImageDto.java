package bsa.java.concurrency.image.dto;

import bsa.java.concurrency.fs.FileSystem;
import java.io.File;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@Getter
@AllArgsConstructor
public class SaveImageDto {
  private final UUID id;
  @NonNull
  private final Long hash;
  private final String url;
}





