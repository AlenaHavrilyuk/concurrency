package bsa.java.concurrency.image.dto;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchResult implements SearchResultDTO{
  UUID imageId;
  Double matchPercent;
  String imageUrl;
}
