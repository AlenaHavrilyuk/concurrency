package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.hash.HashService;
import bsa.java.concurrency.image.dto.SaveImageDto;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.persistent.Persistent;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {
  @Autowired
  FileSystem fileSystem;

  @Autowired
  Persistent persistent;

  @Autowired
  HashService hashService;

  private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

  public void batchUploadImages(MultipartFile multipartFile){
      //one thread for each image

      byte[] byteFile;
      try {
         byteFile = multipartFile.getBytes();
      } catch (IOException e){
        logger.error("Cannot parse incoming file");
        return;
      }
      UUID uuid = UUID.randomUUID();

      //one more thread for saving to the disk
      Future<String> saveToDiskFuture = fileSystem.saveFile(byteFile, uuid);

      //one more thread for getting hash
      Future<Long> hashFuture = hashService.calculateHashAsynch(byteFile);

      //wait for saving to disk and getting hash and create dto for saving to persistent
      SaveImageDto img;
      try {
        img = new SaveImageDto(uuid, hashFuture.get(), saveToDiskFuture.get());
        logger.info("New image is saved to disk");
      } catch (InterruptedException | ExecutionException | NullPointerException e){
        logger.error("Cannot save image to disk");
        return;
      }
      //save to persistent
      persistent.save(img);
      logger.info("New image is saved to persistent");
  }

  public Optional<List<SearchResultDTO>> searchMatches(MultipartFile file, double threshold){
    long newImgHash;
    byte[] byteFile;
    try {
      byteFile = file.getBytes();
    } catch (IOException e){
      logger.error("Cannot parse incoming file");
      return Optional.empty();
    }

    try {
      newImgHash = hashService.calculateHashAsynch(byteFile).get();
    } catch (InterruptedException | ExecutionException e) {
      return Optional.empty();
    }

    List<SearchResultDTO> resultDTOList = persistent.search(newImgHash, threshold);
    if (resultDTOList.isEmpty()) {
      return Optional.empty();
    }

    UUID newImgId = UUID.randomUUID();
    Future<String> saveToDiskFuture = fileSystem.saveFile(byteFile, newImgId);

    try {
      persistent.save(new SaveImageDto(newImgId, newImgHash, saveToDiskFuture.get()));
      logger.info("New image is saved to disk");
      logger.info("New image is saved to persistent");
    } catch (InterruptedException | ExecutionException e) {
      logger.error("Cannot save image to disk");
    }

    return Optional.of(resultDTOList);
  }

  public void delete(UUID imageId) {
    Future<String> deleteFromDiskFuture = fileSystem.delete(imageId);
    persistent.delete(imageId);
    try {
      logger.info(deleteFromDiskFuture.get());
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }

  public void deleteAll() {
    persistent.deleteAll();
    logger.info("Persistent was cleared");
    try {
      fileSystem.deleteAll().get();
    } catch (InterruptedException | ExecutionException e) {
      logger.error("Cannot delete image from disk");
    }
    logger.info("All images from file system was deleted.");
  }
}
