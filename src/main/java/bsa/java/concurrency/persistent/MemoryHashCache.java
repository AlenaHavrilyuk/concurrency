package bsa.java.concurrency.persistent;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SaveImageDto;
import bsa.java.concurrency.image.dto.SearchResult;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MemoryHashCache implements Persistent {
  private Map<UUID, Long> images = new HashMap<>();
  @Autowired
  FileSystem fileSystem;

  @Override
  public void save(SaveImageDto img) {
    images.put(img.getId(), img.getHash());
  }

  @Override
  public List<SearchResultDTO> search(Long newImgHash, double threshold) {
    List<SearchResultDTO> resultDTOList = new ArrayList<>();
    images.forEach((uuid, hash) -> {
      double diffPercent = (double) Long.bitCount(newImgHash ^ hash) / 64;
      double matchPercent = 1 - diffPercent;
      if (matchPercent >= threshold) {
        String imgURL = fileSystem.getHTTPUrlById(uuid);
        resultDTOList.add(new SearchResult(uuid, matchPercent, imgURL));
      }
    });
    return resultDTOList;
  }

  @Override
  public void delete(UUID imageId) {
    images.remove(imageId);
  }

  @Override
  public void deleteAll() {
    images.clear();
  }


}
