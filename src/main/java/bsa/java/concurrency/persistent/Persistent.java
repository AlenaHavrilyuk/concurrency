package bsa.java.concurrency.persistent;

import bsa.java.concurrency.image.dto.SaveImageDto;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface Persistent {
  void save(SaveImageDto img);

  List<SearchResultDTO> search(Long hash, double threshold);

  void delete(UUID imageId);

  void deleteAll();
}
